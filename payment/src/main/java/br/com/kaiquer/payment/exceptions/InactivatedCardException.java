package br.com.kaiquer.payment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Pagamento não efetuado, seu cartão encontra-se inválido")
public class InactivatedCardException extends RuntimeException {
}
