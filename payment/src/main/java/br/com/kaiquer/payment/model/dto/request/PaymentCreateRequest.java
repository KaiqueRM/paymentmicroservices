package br.com.kaiquer.payment.model.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class PaymentCreateRequest {

    @NotBlank
    @JsonProperty(value = "cartao_id")
    private Long cardId;

    @JsonProperty(value = "descricao")
    private String description;

    @JsonProperty(value = "valor")
    private double price;

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
