package br.com.kaiquer.payment.clients.card;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardDTO {
    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "numero")
    private String number;

    @JsonProperty(value = "clienteId")
    private Long userId;

    @JsonProperty(value = "ativo")
    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
