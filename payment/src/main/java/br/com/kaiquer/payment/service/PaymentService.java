package br.com.kaiquer.payment.service;

import br.com.kaiquer.payment.clients.card.CardClient;
import br.com.kaiquer.payment.clients.card.CardDTO;
import br.com.kaiquer.payment.clients.card.exceptions.CardNotFoundException;
import br.com.kaiquer.payment.exceptions.InactivatedCardException;
import br.com.kaiquer.payment.model.Payment;
import br.com.kaiquer.payment.repository.PaymentRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CardClient cardClient;

    public Payment create (Payment payment) {
        CardDTO cardDTO;

        try {
            cardDTO = cardClient.getCardById(payment.getCardId());
        } catch (FeignException.FeignClientException.NotFound notFound) {
            throw new CardNotFoundException();
        }

        if(!cardDTO.isActive()) {
            throw new InactivatedCardException();
        }
        System.out.println("Pagamento criado "+System.currentTimeMillis());
        return paymentRepository.save(payment);
    }

    public List<Payment> getPaymentsByCardId(Long cardId) {
        List<Payment> payments = paymentRepository.findByCardId(cardId);
        System.out.println("Pagamento obtido "+System.currentTimeMillis());
        return payments;
    }
}
