package br.com.kaiquer.payment.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentDetailResponse {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "cartao_id")
    private Long cardId;

    @JsonProperty(value = "descricao")
    private String description;

    @JsonProperty(value = "valor")
    private double price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
