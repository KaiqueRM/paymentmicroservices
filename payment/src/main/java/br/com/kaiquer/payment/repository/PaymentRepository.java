package br.com.kaiquer.payment.repository;

import br.com.kaiquer.payment.model.Payment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {
    @Query("SELECT p FROM Payment p WHERE p.cardId = ?1")
    List<Payment> findByCardId(Long cardId);
}
