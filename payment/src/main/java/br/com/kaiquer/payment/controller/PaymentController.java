package br.com.kaiquer.payment.controller;

import br.com.kaiquer.payment.model.Payment;
import br.com.kaiquer.payment.model.PaymentMapper;
import br.com.kaiquer.payment.model.dto.request.PaymentCreateRequest;
import br.com.kaiquer.payment.model.dto.response.PaymentDetailResponse;
import br.com.kaiquer.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="pagamento")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentMapper mapper;

    @PostMapping()
    @ResponseStatus(value = HttpStatus.CREATED)
    public PaymentDetailResponse newPayment(@RequestBody PaymentCreateRequest paymentCreateRequest) {
        Payment payment = mapper.toPayment(paymentCreateRequest);
        return mapper.toPaymentDetailResponse(paymentService.create(payment));
    }

    @GetMapping("/{id_cartao}")
    public List<Payment> paymentsFromCard(@PathVariable(value = "id_cartao") Long cardId) {
        return paymentService.getPaymentsByCardId(cardId);
    }
}
