package br.com.kaiquer.payment.clients.card.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Cartão não encontrado")
public class CardNotFoundException extends RuntimeException {
}
