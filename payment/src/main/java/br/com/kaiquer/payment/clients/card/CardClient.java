package br.com.kaiquer.payment.clients.card;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "cartao")
public interface CardClient {

    @GetMapping("/cartao/id/{id}")
    CardDTO getCardById(@PathVariable(value = "id") Long id);

}
