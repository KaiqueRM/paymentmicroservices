package br.com.kaiquer.payment.model;

import br.com.kaiquer.payment.model.dto.request.PaymentCreateRequest;
import br.com.kaiquer.payment.model.dto.response.PaymentDetailResponse;
import org.springframework.stereotype.Component;

@Component
public class PaymentMapper {

    public Payment toPayment(PaymentCreateRequest paymentCreateRequest) {
        if (paymentCreateRequest == null) return null;

        Payment payment = new Payment();
        payment.setCardId(paymentCreateRequest.getCardId());
        payment.setDescription(paymentCreateRequest.getDescription());
        payment.setPrice(paymentCreateRequest.getPrice());

        return payment;
    }

    public PaymentDetailResponse toPaymentDetailResponse(Payment payment) {
        if(payment == null) return null;

        PaymentDetailResponse paymentDetailResponse = new PaymentDetailResponse();
        paymentDetailResponse.setId(payment.getId());
        paymentDetailResponse.setCardId(payment.getCardId());
        paymentDetailResponse.setDescription(payment.getDescription());
        paymentDetailResponse.setPrice(payment.getPrice());

        return paymentDetailResponse;
    }

}
