package br.com.kaiquer.client.repository;

import br.com.kaiquer.client.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
}
