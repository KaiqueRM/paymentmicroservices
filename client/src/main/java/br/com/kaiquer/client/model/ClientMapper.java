package br.com.kaiquer.client.model;

import br.com.kaiquer.client.model.dto.request.CreateClientRequest;
import br.com.kaiquer.client.model.dto.response.ClientCreatedResponse;
import br.com.kaiquer.client.model.dto.response.ClientDetailsResponse;
import org.springframework.stereotype.Component;

@Component
public class ClientMapper {

    public Client toClient(CreateClientRequest createClientRequest) {
        Client client = new Client();
        client.setName(createClientRequest.getName());
        return client;
    }

    public ClientCreatedResponse toClienteCreatedResponse(Client client) {
        ClientCreatedResponse clientCreatedResponse = new ClientCreatedResponse();
        clientCreatedResponse.setId(client.getId());
        clientCreatedResponse.setName(client.getName());
        return clientCreatedResponse;
    }

    public ClientDetailsResponse toClienteDetailsResponse(Client client) {
        ClientDetailsResponse clientDetailsResponse = new ClientDetailsResponse();
        clientDetailsResponse.setId(client.getId());
        clientDetailsResponse.setName(client.getName());
        return clientDetailsResponse;
    }

}
