package br.com.kaiquer.client.model.dto.request;

import javax.validation.constraints.NotBlank;

public class CreateClientRequest {

    @NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
