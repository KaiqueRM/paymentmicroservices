package br.com.kaiquer.client.service;

import br.com.kaiquer.client.exceptions.ClientNotFoundException;
import br.com.kaiquer.client.model.Client;
import br.com.kaiquer.client.model.ClientMapper;
import br.com.kaiquer.client.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepository;

    public Client create (Client client) {
        System.out.println("Cliente criado "+System.currentTimeMillis());
        return clientRepository.save(client);
    }

    public Client findById(Long id){
        System.out.println("procurando cliente"+System.currentTimeMillis());
        Optional<Client> client = clientRepository.findById(id);
        if(!client.isPresent()) { throw new ClientNotFoundException(); }
        return client.get();
    }
}
