package br.com.kaiquer.client.controller;

import br.com.kaiquer.client.model.Client;
import br.com.kaiquer.client.model.ClientMapper;
import br.com.kaiquer.client.model.dto.request.CreateClientRequest;
import br.com.kaiquer.client.model.dto.response.ClientCreatedResponse;
import br.com.kaiquer.client.model.dto.response.ClientDetailsResponse;
import br.com.kaiquer.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/cliente")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientMapper clientMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClientCreatedResponse newClient(@Valid @RequestBody CreateClientRequest createClientRequest){
        Client client = clientMapper.toClient(createClientRequest);
        client = clientService.create(client);
        return clientMapper.toClienteCreatedResponse(client);
    }

    @GetMapping("/{id}")
    public ClientDetailsResponse findById(@PathVariable(value = "id") Long clientId) {
        System.out.println("Consultando Cliente "+System.currentTimeMillis());
        Client client = clientService.findById(clientId);
        return clientMapper.toClienteDetailsResponse(client);
    }
}
