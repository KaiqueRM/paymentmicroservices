package br.com.kaiquer.card.service;

import br.com.kaiquer.card.clients.client.ClientClient;
import br.com.kaiquer.card.clients.client.exceptions.ClientClientNotFoundException;
import br.com.kaiquer.card.exceptions.CardAlreadyTakenException;
import br.com.kaiquer.card.exceptions.CardNotFoundException;
import br.com.kaiquer.card.models.Card;
import br.com.kaiquer.card.repository.CardRepository;
import br.com.kaiquer.card.clients.client.dto.ClientDTO;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CardService {
    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private ClientClient clientClient;

    public Card create(Card card) {
        if(cardRepository.findByNumber(card.getNumber()).isPresent()) {
            throw new CardAlreadyTakenException();
        }
        try {
            ClientDTO client = clientClient.findById(card.getUserId());
        } catch (FeignException.FeignClientException.NotFound notFound) {
            throw new ClientClientNotFoundException();
        }
        return cardRepository.save(card);
    }

    public Card updateActive(String cardNumber, boolean isActive) {
        Optional<Card> byNumber = cardRepository.findByNumber(cardNumber);

        if(!byNumber.isPresent()) {
            throw new CardNotFoundException();
        }

        Card card = byNumber.get();
        card.setActive(isActive);
        return cardRepository.save(card);
    }

    public Card findByNumber(String cardNumber) {
        Optional<Card> card =  cardRepository.findByNumber(cardNumber);
        return validatePresentCard(card);
    }

    public Card findById(Long id) {
        Optional<Card> card = cardRepository.findById(id);
        return validatePresentCard(card);
    }

    private Card validatePresentCard(Optional<Card> card) {
        if(!card.isPresent()) {
            throw new CardNotFoundException();
        }
        return card.get();
    }



}