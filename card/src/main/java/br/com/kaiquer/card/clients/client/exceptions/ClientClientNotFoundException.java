package br.com.kaiquer.card.clients.client.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cliente especificado não foi encontrado :c")
public class ClientClientNotFoundException extends RuntimeException {
}
