package br.com.kaiquer.card.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Já existe um cartão com o número informado")
public class CardAlreadyTakenException extends RuntimeException {
}
