package br.com.kaiquer.card.repository;

import br.com.kaiquer.card.models.Card;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CardRepository extends CrudRepository<Card, Long> {

    Optional<Card> findByNumber(String number);
}
