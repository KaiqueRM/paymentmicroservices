package br.com.kaiquer.card.controller;

import br.com.kaiquer.card.models.Card;
import br.com.kaiquer.card.models.CardMapper;
import br.com.kaiquer.card.models.dto.request.CardActivateRequest;
import br.com.kaiquer.card.models.dto.request.CardCreateRequest;
import br.com.kaiquer.card.models.dto.response.CardDetailResponse;
import br.com.kaiquer.card.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartao")
public class CardController {

    @Autowired
    CardService cardService;

    @Autowired
    CardMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CardDetailResponse newCard(@RequestBody CardCreateRequest cardCreateRequest) {
        Card card = mapper.toCard(cardCreateRequest);
        card = cardService.create(card);
        return mapper.toCardDetailResponse(card);
    }

    @PatchMapping("/{numero}")
    public CardDetailResponse setActive(@PathVariable(value = "numero") String number, @RequestBody() CardActivateRequest cardActivateRequest) {
        return mapper.toCardDetailResponse(cardService.updateActive(number, cardActivateRequest.getActive()));
    }

    @GetMapping("/{numero}")
    public CardDetailResponse getCardByNumber(@PathVariable(value = "numero") String cardNumber) {
        return mapper.toCardDetailResponse(cardService.findByNumber(cardNumber));
    }

    @GetMapping("/id/{id}")
    public CardDetailResponse getCardById(@PathVariable(value = "id") Long id) {
        return mapper.toCardDetailResponse(cardService.findById(id));
    }
}
