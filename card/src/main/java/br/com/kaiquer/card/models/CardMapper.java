package br.com.kaiquer.card.models;

import br.com.kaiquer.card.models.dto.request.CardCreateRequest;
import br.com.kaiquer.card.models.dto.response.CardDetailResponse;
import org.springframework.stereotype.Component;

@Component
public class CardMapper {

    public CardDetailResponse toCardDetailResponse(Card card) {

        if(card == null) return null;
        CardDetailResponse cardResponse = new CardDetailResponse();
        cardResponse.setId(card.getId());
        cardResponse.setActive(card.isActive());
        cardResponse.setNumber(card.getNumber());
        cardResponse.setUserId(card.getUserId());

        return cardResponse;
    }

    public Card toCard(CardCreateRequest cardCreateRequest) {

        if(cardCreateRequest == null) return null;
        Card card = new Card();
        card.setActive(cardCreateRequest.isActive());
        card.setNumber(cardCreateRequest.getNumber());
        card.setUserId(cardCreateRequest.getUserId());

        return card;
    }
}
