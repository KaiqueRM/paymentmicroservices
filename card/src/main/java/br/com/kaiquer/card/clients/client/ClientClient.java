package br.com.kaiquer.card.clients.client;

import br.com.kaiquer.card.clients.client.dto.ClientDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface ClientClient {

    @GetMapping("/cliente/{id}")
    ClientDTO findById(@PathVariable(value = "id") Long clientId);
}
