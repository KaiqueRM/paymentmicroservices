package br.com.kaiquer.card.models.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardCreateRequest {
    @JsonProperty(value = "numero")
    private String number;

    @JsonProperty(value = "clienteId")
    private Long userId;

    @JsonProperty(value = "ativo")
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
