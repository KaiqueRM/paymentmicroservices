package br.com.kaiquer.receipt.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardReceiptCreateRequest {

    @JsonProperty("cartao_id")
    private Long cardId;

    @JsonProperty("valor_fatura")
    private Double amount;

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
