package br.com.kaiquer.receipt.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="fatura_cartao")
public class CardReceipt {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_cartao")
    private Long cardId;

    @Column(name = "valor_fatura")
    private Double amount;

    @Column(name = "data_vencimento")
    private Date dueDate;

    @Column(name = "data_referencia")
    private Date referDate;

    @Column(name = "ativa")
    private Boolean isActive;

    @Column(name = "data_pagamento")
    private Date paymentDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCard() {
        return cardId;
    }

    public void setCard(Long card) {
        this.cardId = card;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getReferDate() {
        return referDate;
    }

    public void setReferDate(Date referDate) {
        this.referDate = referDate;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }
}
