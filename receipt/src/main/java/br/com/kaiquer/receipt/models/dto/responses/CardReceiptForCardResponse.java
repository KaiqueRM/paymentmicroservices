package br.com.kaiquer.receipt.models.dto.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class CardReceiptForCardResponse {

    @JsonProperty(value = "id_fatura")
    private Long id;

    @JsonProperty(value = "data_vencimento")
    private Date dueDate;

}
